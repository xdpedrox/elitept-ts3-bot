import { ts3, ts3Core } from "../lib/ts3-lib/src/index"
import db from "../lib/shared/db"
import { registerUsers } from "../services/registerUsers"
import { resetPerm } from "./resetPermissions"
const startCid = "2"
const endCid = "3"

const importChannels = async () => {
  const channels = await ts3.channelList()

  const teams: (typeof channels)[] = []

  let started = false
  let ended = false
  let spacerCount = -1
  let currentTeamIndex = 0

  teams[0] = []

  for (const c of channels) {
    if (c.cid == startCid) {
      started = true
      continue
    }
    if (c.cid == endCid) {
      ended = true
      continue
    }

    if (started == true && ended == false) {
      // @ts-ignore
      if (ts3Core.channelIsSpacer(await c.getInfo())) {
        spacerCount++

        if (spacerCount % 2 === 0) {
          teams[currentTeamIndex].push(c)
        } else {
          console.log("Spacer un even", c.name)
          currentTeamIndex++
          teams[currentTeamIndex] = []
        }
      } else {
        teams[currentTeamIndex].push(c)
      }
    }
  }

  // Removing last empty array
  teams.pop()

  for (const teamChannels of teams) {
    const channelGroups: {
      name: string
      cgid: string
      permission: number
      users: string[]
    }[] = [
      {
        name: "Dono",
        cgid: "12",
        users: [],
        permission: 1,
      },
      {
        name: "Admin",
        cgid: "13",
        users: [],
        permission: 2,
      },
      {
        name: "Mod",
        cgid: "14",
        users: [],
        permission: 3,
      },
      {
        name: "User",
        cgid: "15",
        users: [],
        permission: 4,
      },
    ]

    const mainChannel = teamChannels[0]
    const teamName = mainChannel.name.split("♦")[1]

    for (const channel of teamChannels) {
      for (const channelGroup of channelGroups) {
        try {
          const groupMembers = await ts3.channelGroupClientList(channelGroup.cgid, channel.cid)
          for (const groupMember of groupMembers) {
            if (!channelGroup.users.find((u) => u == groupMember.cldbid)) {
              channelGroup.users.push(groupMember.cldbid)
            }
          }
        } catch (err) {
          if (err.id != "1281") {
            console.log(err)
          }
        }
      }
    }

    if (channelGroups[0].users.length == 0) {
      const admins = [...channelGroups[1].users]
      channelGroups[0].users = admins
      channelGroups[1].users = []
    }

    const alreadyPresentUsers = []

    const teamMembersIds: {
      permission: number
      uuid: string
    }[] = []

    channelGroups.map(async (cg) => {
      const users = []

      for (const u of cg.users) {
        if (!alreadyPresentUsers.includes(u)) {
          alreadyPresentUsers.push(u)
          const client = await ts3.clientGetNameFromDbid(u)
          teamMembersIds.push({
            permission: cg.permission,
            uuid: client.cluid,
          })
        }
      }

      return {
        ...cg,
        users,
      }
    })

    console.log("channelGroups", teamMembersIds)

    const team = await db.team.upsert({
      where: { cid: +mainChannel.cid },
      create: { name: teamName, cid: +mainChannel.cid, active: true },
      update: { name: teamName },
    })

    for (const teamMember of teamMembersIds) {
      const user = await registerUsers({
        nickname: "",
        uniqueIdentifier: teamMember.uuid,
      })

      const teamUser = await db.teamUser.upsert({
        where: { teamId_userId: { teamId: team.id, userId: user.id } },
        create: {
          active: true,
          permission: teamMember.permission,
          teamId: team.id,
          userId: user.id,
        },
        update: {
          active: true,
          permission: teamMember.permission,
        },
      })
    }
    const createdTeam = await db.team.findMany({
      where: { name: teamName },
      select: {
        id: true,
        name: true,
        teamUsers: {
          select: {
            id: true,
            permission: true,
            user: {
              select: {
                id: true,
                name: true,
                nickname: true,
                registered: true,
                teamSpeakClients: { select: { id: true, uuid: true } },
              },
            },
          },
        },
      },
    })
    await resetPerm(team.id)
    console.dir(createdTeam, { depth: null })
  }
}

importChannels()
