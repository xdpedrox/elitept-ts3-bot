import db from "../lib/shared/db"

import { ts3, ts3Core, teamSetPermission } from "../lib/ts3-lib/src/index"
import { publicChannelProperties } from "../lib/shared/config/channels"
// ../lib/ts3/src/config/channels"
import { channelSettings, groupSettings } from "../lib/shared/config/ts3config"
import { uniq, groupBy } from "rambda"

export const resetPerm = async (teamId: number) => {
  try {
    const team = await db.team.findFirst({
      where: { id: teamId },
      include: {
        teamUsers: { include: { user: { include: { teamSpeakClients: true } } } },
      },
    })

    if (team.serverGroupId) {
      // Reset group permissions
      console.log("getting Server grouip")
      const serverGroup = await ts3.getServerGroupById(`${team.serverGroupId}`)

      // Remove all users from the group
      console.log("getting Server group client list")
      try {
        const clientList = await serverGroup?.clientList()
        if (clientList) {
          for (const client of clientList) {
            console.log("removing user server group")
            await serverGroup?.delClient(client.cldbid)
          }
        }
      } catch (err) {
        if (err.id == "1281") {
          console.log("clientList Empty")
        }
      }
    }

    // Removing Channel permissions
    console.log("Get subchannel list")
    const channeList = await ts3Core.subChannelList(`${team.cid}`)

    for (const channel of channeList) {
      // Dono
      await fetchChannelGroupAndRemove(groupSettings.channelOwner, channel.cid)
      // Admin
      await fetchChannelGroupAndRemove(groupSettings.channelAdmin, channel.cid)
      // Mod
      await fetchChannelGroupAndRemove(groupSettings.channelMod, channel.cid)
      // Member
      await fetchChannelGroupAndRemove(groupSettings.channelMember, channel.cid)
    }

    console.log("start adding")
    // Re-adding the permissions
    const users = team.teamUsers
      .map((tu) =>
        tu.user.teamSpeakClients.map((tsc) => ({
          permission: tu.permission,
          uuid: tsc.uuid,
        }))
      )
      .flat()

    const usersGrouped = groupBy((i) => `${i.permission}`, users)

    for (const permission of Object.keys(usersGrouped)) {
      const userss = usersGrouped[permission]
      if (userss) {
        const uuids = userss?.map((u) => u.uuid).filter((uuid) => uuid)
        console.log("Adding user")
        await teamSetPermission(`${team.cid}`, uuids, Number(permission), team.serverGroupId)
      }
    }

    return {
      success: true,
      message: "Permissions Reset",
    }
  } catch (err) {
    console.log("failed to reset", err)
    return {
      success: false,
      message: "Something happened",
    }
  }
}

const fetchChannelGroupAndRemove = async (channelGroupId, cid) => {
  try {
    const ownerUsers = await ts3.channelGroupClientList(channelGroupId, cid)
    const cldbids = ownerUsers.map((u) => u.cldbid)
    for (const cldbid of cldbids) {
      if (cldbid) {
        await ts3.setClientChannelGroup(groupSettings.guestChannelGroup, cid, cldbid)
      }
    }
  } catch (err) {
    if (err.id == "1281") {
      console.log("clientList Empty")
    }
  }
}
