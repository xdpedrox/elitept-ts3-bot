import { registerUsers } from "./services/registerUsers"
import { addChannelPermToDefaultChannel } from "./services/addChannelPermToDefaultChannel"
import { checkChannelUse } from "./services/checkChannelUse"
import { setTrashIconToUnsuedTeams } from "./services/setTrashIconToUnsuedTeams"
import { CronJob } from "cron"

import { ts3 } from "./lib/ts3-lib/src"

ts3.on("clientconnect", async ({ client }) => {
  await registerUsers(client)
  await addChannelPermToDefaultChannel(client)
})

const job = new CronJob(
  "*/5 * * * *", // cronTime
  async () => {
    await checkChannelUse()
    await setTrashIconToUnsuedTeams()
  }, // onTick
  null, // onComplete
  true, // start
  "Europe/London" // timeZone
)
