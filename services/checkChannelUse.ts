import dotenv from "dotenv"
dotenv.config({ path: "../../.env" })
import prisma from "../db"
import { ts3, ts3Core } from "../lib/ts3-lib/src"
import { groupBy } from "rambda"

export const checkChannelUse = async () => {
  const teams = await prisma.team.findMany({ where: { active: true } })
  const channelList = await ts3.channelList()

  // Get all team Cids
  const teamCids = teams.map((t) => t.cid)

  // Get subChannels that are part of a team and have people inside.
  const teamSubChannels = channelList.filter(
    (channel) => teamCids.includes(+channel.pid) && channel.totalClients > 0
  )

  // Group Channels by ParentId / team maid cid
  const channelsByTeam = groupBy((c) => c.pid, teamSubChannels)
  
  // get the keys of that group. All pcid of channels in use.
  const cidsInUse = Object.keys(channelsByTeam).map((id) => Number(id))

  // Update all teams currently in use.
  await prisma.team.updateMany({
    where: { cid: { in: cidsInUse } },
    data: { lastUsedAt: new Date() },
  })

  console.log("Updated used channels")
}

