import { ts3, ts3Core } from "../lib/ts3-lib/src"
import fs from "fs"
import { groups } from "../lib/shared/config/groups"
import FileType from "file-type-cjs-fix"
import path from "path"

import dotenv from "dotenv"
dotenv.config({ path: "../../.env" })
import prisma from "../db"

// To be run once per day

const iconExtension = (type: { ext: string; mime: string }) => {
  return type.ext == "xml" ? "svg" : type.ext
}

// Function to compare two buffers
function areBuffersEqual(buffer1, buffer2) {
  if (buffer1.length !== buffer2.length) {
    return false
  }

  for (let i = 0; i < buffer1.length; i++) {
    if (buffer1[i] !== buffer2[i]) {
      return false
    }
  }

  return true
}

const downloadServerIcons = async () => {
  const groupList = await ts3.serverGroupList()

  const toAddGroups = groupList.filter((gl) => groups.includes(Number(gl.sgid)))

  const groupIconFolder = "public/images/groups"

  for (const serverGroup of toAddGroups) {
    const iconBuffer = await serverGroup.getIcon()
    const iconId = await serverGroup.getIconId()
    const type = await FileType.fromBuffer(iconBuffer)

    const extension = iconExtension(type)
    const filePath = `${groupIconFolder}/${iconId}.${extension}`
    const absoluteFilePath = path.join(__dirname, "../../", filePath)

    const serverGroupDb = await prisma.teamSpeakServerGroup.findFirst({
      where: { sgid: Number(serverGroup.sgid) },
    })

    if (!serverGroupDb) {
      await prisma.teamSpeakServerGroup.create({
        data: {
          sgid: Number(serverGroup.sgid),
          fileUrl: filePath,
          type: "GROUP",
        },
      })
      await fs.writeFileSync(absoluteFilePath, iconBuffer)
      console.log("No Icon adding new")
    } else if (serverGroupDb.fileUrl == filePath) {
      console.log("Same path")
      try {
        const existingFileBuffer = await fs.promises.readFile(absoluteFilePath)
        // Compare the buffers
        if (!areBuffersEqual(existingFileBuffer, iconBuffer)) {
          await fs.writeFileSync(absoluteFilePath, iconBuffer)
          console.log("Files don't match")
        }
      } catch (err) {
        console.log("Files not found downloading")
        await fs.writeFileSync(absoluteFilePath, iconBuffer)
      }
    } else {
      // Delete Icon
      const oldAbsoluteFilePath = path.join(__dirname, "../../", serverGroupDb.fileUrl)

      await fs.promises
        .unlink(oldAbsoluteFilePath)
        .then(() => console.log("File deleted successfully."))
        .catch((err) => console.log("file not found"))
      await fs.writeFileSync(absoluteFilePath, iconBuffer)
      await prisma.teamSpeakServerGroup.update({
        where: {
          id: serverGroupDb.id,
        },
        data: {
          fileUrl: filePath,
        },
      })
      console.log("Icon extensions aren't the same deleting and adding")
    }
  }
}

void downloadServerIcons()
