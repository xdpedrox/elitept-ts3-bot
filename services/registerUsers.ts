import dotenv from "dotenv"
dotenv.config({ path: "../../.env" })
import prisma from "../db"
import { TeamSpeakClient } from "ts3-nodejs-library"

export const registerUsers = async (client: { nickname: string; uniqueIdentifier: string }) => {
  const connectClientName = client.nickname.trim()

  const tsc = await prisma.teamSpeakClient.findFirst({
    where: { uuid: client.uniqueIdentifier },
    select: { id: true, user: { select: { id: true, nickname: true } } },
  })

  console.log("tsc", tsc)
  // Check if TeamSpeak Client Exists
  if (tsc) {
    console.log("User Connected - Client Already Registered")

    // If there is no user assigned to this teamSpeak Client - Create new User
    if (!tsc.user) {
      const user = await prisma.user.create({
        data: { teamSpeakClients: { connect: { id: tsc.id } }, nickname: client.nickname },
        select: { id: true, nickname: true } 
      })
      console.log("Create User to TeamSpeak Client")
      return user
    }
    // User already exist let's Update the NickName
    else {
      const savedClientName = tsc.user.nickname

      if (connectClientName !== savedClientName) {
        await prisma.teamSpeakClient.update({
          where: { uuid: client.uniqueIdentifier },
          data: { user: { update: { nickname: connectClientName } } },
        })
        console.log("Updated NickName")
      }
      return tsc.user
    }
  }
  // Create User + TeamSpeak Client
  else {
    const newTsc = await prisma.teamSpeakClient.create({
      data: {
        uuid: client.uniqueIdentifier,
        user: { create: { nickname: client.nickname } },
      },
      select: { id: true, user: { select: { id: true, nickname: true } } },
    })
    console.log("User Connected - Client Registered")
    return newTsc.user
  }
}
