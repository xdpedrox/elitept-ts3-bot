import dotenv from "dotenv"
dotenv.config({ path: "../../.env" })
import prisma from "../db"
import { ts3, ts3Core } from "../lib/ts3-lib/src"
import { groupBy } from "rambda"
import { subDays } from "date-fns"

export const setTrashIconToUnsuedTeams = async () => {
  const today = new Date()

  const twoWeeksAgo = subDays(today, 14)

  const teams = await prisma.team.findMany({
    where: { active: true },
  })

  for (const team of teams) {
    if (team.lastUsedAt > twoWeeksAgo) {
      await ts3Core.channelSetSubChannelIcon(`${team.cid}`)
    } else {
      await ts3Core.channelSetSubChannelIcon(`${team.cid}`, 583019839)
    }
  }
}

