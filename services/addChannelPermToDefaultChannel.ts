import { ts3, ts3Core } from "../lib/ts3-lib/src"
import { channelSettings, groupSettings } from "../lib/shared/config/ts3config"

import dotenv from "dotenv"
dotenv.config({ path: "../../.env.local" })
import prisma from "../lib/shared/db/index"
import { TeamSpeakClient } from "ts3-nodejs-library"

export const addChannelPermToDefaultChannel = async (client: TeamSpeakClient) => {
  try {
    // Check if they have the group
    await ts3.channelGroupClientList(
      groupSettings.channelMember,
      channelSettings.defaultChannelCid,
      client.databaseId
    )
  } catch (err) {
    // User doesn't have the channel group yet
    if (err.id == "1281") {
      await ts3.setClientChannelGroup(
        groupSettings.channelMember,
        channelSettings.defaultChannelCid,
        client.databaseId
      )
    }
  }
}
