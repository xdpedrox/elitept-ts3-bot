import { PrismaClient } from "@prisma/client"
import dotenv from "dotenv"

dotenv.config({ path: "../.env" })

export * from "@prisma/client"
const db = new PrismaClient()
export default db
